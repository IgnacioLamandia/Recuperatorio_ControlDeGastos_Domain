import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class Filtrador {
  private String descripcion = "";
  
  public List<Gasto> getGastos(final List<Gasto> gastos) {
    final Function1<Gasto, Boolean> _function = new Function1<Gasto, Boolean>() {
      public Boolean apply(final Gasto gasto) {
        String _descripcion = gasto.getDescripcion();
        String _lowerCase = _descripcion.toLowerCase();
        String _lowerCase_1 = Filtrador.this.descripcion.toLowerCase();
        return Boolean.valueOf(_lowerCase.contains(_lowerCase_1));
      }
    };
    Iterable<Gasto> _filter = IterableExtensions.<Gasto>filter(gastos, _function);
    return IterableExtensions.<Gasto>toList(_filter);
  }
  
  @Pure
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }
}
