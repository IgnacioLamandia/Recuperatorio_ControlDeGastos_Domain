import com.google.common.base.Objects;
import java.util.List;
import org.uqbar.commons.model.UserException;

@SuppressWarnings("all")
public class Calculador {
  private Filtrador filtrador;
  
  public Calculador(final Filtrador filtrador) {
    this.filtrador = filtrador;
  }
  
  public int calcularIndice(final Gasto gastoSeleccionado, final List<Gasto> gastos) {
    int _xifexpression = (int) 0;
    if ((Objects.equal(gastoSeleccionado.getDescripcion(), null) && (gastoSeleccionado.getMonto() == 0))) {
      throw new UserException("Seleccione un gasto primero");
    } else {
      int _xblockexpression = (int) 0;
      {
        String _descripcion = gastoSeleccionado.getDescripcion();
        this.filtrador.setDescripcion(_descripcion);
        int _sumarGastosFiltrados = this.sumarGastosFiltrados(gastos);
        int _size = gastos.size();
        _xblockexpression = (_sumarGastosFiltrados / _size);
      }
      _xifexpression = _xblockexpression;
    }
    return _xifexpression;
  }
  
  public int sumarGastosFiltrados(final List<Gasto> gastosFiltrados) {
    int _xblockexpression = (int) 0;
    {
      int res = 0;
      for (final Gasto g : gastosFiltrados) {
        int _monto = g.getMonto();
        int _plus = (res + _monto);
        res = _plus;
      }
      _xblockexpression = res;
    }
    return _xblockexpression;
  }
}
