import java.time.LocalDate;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class Gasto implements Cloneable {
  private String descripcion;
  
  private LocalDate fechaRegistro;
  
  private int monto;
  
  public Gasto(final String descripcion, final int monto) {
    this.descripcion = descripcion;
    LocalDate _now = LocalDate.now();
    this.fechaRegistro = _now;
    this.monto = monto;
  }
  
  public Gasto() {
    this.monto = 0;
  }
  
  public Gasto copy() {
    try {
      Object _clone = super.clone();
      return ((Gasto) _clone);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Pure
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }
  
  @Pure
  public LocalDate getFechaRegistro() {
    return this.fechaRegistro;
  }
  
  public void setFechaRegistro(final LocalDate fechaRegistro) {
    this.fechaRegistro = fechaRegistro;
  }
  
  @Pure
  public int getMonto() {
    return this.monto;
  }
  
  public void setMonto(final int monto) {
    this.monto = monto;
  }
}
