import com.google.common.base.Objects;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.model.UserException;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class Usuario implements Cloneable {
  private String nombre;
  
  private int gastoTotal;
  
  private List<Gasto> gastos;
  
  private Filtrador filtrador;
  
  public Usuario(final String nombre) {
    this.nombre = nombre;
    this.gastoTotal = 0;
    ArrayList<Gasto> _arrayList = new ArrayList<Gasto>();
    this.gastos = _arrayList;
    Filtrador _filtrador = new Filtrador();
    this.filtrador = _filtrador;
  }
  
  public int nuevoGasto(final Gasto gasto) {
    int _xifexpression = (int) 0;
    if (((!Objects.equal(gasto.getDescripcion(), "")) && (gasto.getMonto() != 0))) {
      int _xblockexpression = (int) 0;
      {
        this.gastos.add(gasto);
        int _monto = gasto.getMonto();
        int _plus = (this.gastoTotal + _monto);
        _xblockexpression = this.gastoTotal = _plus;
      }
      _xifexpression = _xblockexpression;
    } else {
      throw new UserException("Gasto invalido");
    }
    return _xifexpression;
  }
  
  public Usuario copy() {
    try {
      Object _clone = super.clone();
      return ((Usuario) _clone);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Pure
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }
  
  @Pure
  public int getGastoTotal() {
    return this.gastoTotal;
  }
  
  public void setGastoTotal(final int gastoTotal) {
    this.gastoTotal = gastoTotal;
  }
  
  @Pure
  public List<Gasto> getGastos() {
    return this.gastos;
  }
  
  public void setGastos(final List<Gasto> gastos) {
    this.gastos = gastos;
  }
  
  @Pure
  public Filtrador getFiltrador() {
    return this.filtrador;
  }
  
  public void setFiltrador(final Filtrador filtrador) {
    this.filtrador = filtrador;
  }
}
