import java.time.LocalDate;
import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.model.ObservableUtils;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class ControlDeGastosAppModel {
  private RepoUsuarios repoUsuarios;
  
  private Usuario usuarioLogeado;
  
  private Filtrador filtrador;
  
  private Calculador calculador;
  
  private Gasto gastoSeleccionado;
  
  private List<Gasto> gastos;
  
  private Gasto gasto = new Gasto();
  
  private int gastoParcial;
  
  private int indice;
  
  public ControlDeGastosAppModel(final RepoUsuarios repo) {
    this.repoUsuarios = repo;
    Filtrador _filtrador = new Filtrador();
    this.filtrador = _filtrador;
    Gasto _gasto = new Gasto();
    this.gastoSeleccionado = _gasto;
    Calculador _calculador = new Calculador(this.filtrador);
    this.calculador = _calculador;
  }
  
  public void filtrar() {
    ObservableUtils.firePropertyChanged(this, "gastos");
    ObservableUtils.firePropertyChanged(this, "sumarGastosFiltrados");
  }
  
  public List<Gasto> getGastos() {
    List<Gasto> _gastos = this.usuarioLogeado.getGastos();
    return this.filtrador.getGastos(_gastos);
  }
  
  public int sumarGastosFiltrados() {
    int _xblockexpression = (int) 0;
    {
      List<Gasto> gastosFiltrados = this.getGastos();
      _xblockexpression = this.calculador.sumarGastosFiltrados(gastosFiltrados);
    }
    return _xblockexpression;
  }
  
  public Usuario logearUsuario(final Usuario usuario) {
    return this.usuarioLogeado = usuario;
  }
  
  public int agregar() {
    int _xblockexpression = (int) 0;
    {
      LocalDate _now = LocalDate.now();
      this.gasto.setFechaRegistro(_now);
      this.usuarioLogeado.nuevoGasto(this.gasto);
      Gasto _gasto = new Gasto();
      this.gasto = _gasto;
      this.filtrar();
      int _gastoTotal = this.usuarioLogeado.getGastoTotal();
      _xblockexpression = this.gastoParcial = _gastoTotal;
    }
    return _xblockexpression;
  }
  
  public int calcularIndice() {
    int _xblockexpression = (int) 0;
    {
      List<Gasto> _gastos = this.getGastos();
      this.gastos = _gastos;
      int _calcularIndice = this.calculador.calcularIndice(this.gastoSeleccionado, this.gastos);
      _xblockexpression = this.indice = _calcularIndice;
    }
    return _xblockexpression;
  }
  
  public int inicializar() {
    int _xblockexpression = (int) 0;
    {
      this.filtrar();
      int _gastoTotal = this.usuarioLogeado.getGastoTotal();
      _xblockexpression = this.gastoParcial = _gastoTotal;
    }
    return _xblockexpression;
  }
  
  @Pure
  public RepoUsuarios getRepoUsuarios() {
    return this.repoUsuarios;
  }
  
  public void setRepoUsuarios(final RepoUsuarios repoUsuarios) {
    this.repoUsuarios = repoUsuarios;
  }
  
  @Pure
  public Usuario getUsuarioLogeado() {
    return this.usuarioLogeado;
  }
  
  public void setUsuarioLogeado(final Usuario usuarioLogeado) {
    this.usuarioLogeado = usuarioLogeado;
  }
  
  @Pure
  public Filtrador getFiltrador() {
    return this.filtrador;
  }
  
  public void setFiltrador(final Filtrador filtrador) {
    this.filtrador = filtrador;
  }
  
  @Pure
  public Calculador getCalculador() {
    return this.calculador;
  }
  
  public void setCalculador(final Calculador calculador) {
    this.calculador = calculador;
  }
  
  @Pure
  public Gasto getGastoSeleccionado() {
    return this.gastoSeleccionado;
  }
  
  public void setGastoSeleccionado(final Gasto gastoSeleccionado) {
    this.gastoSeleccionado = gastoSeleccionado;
  }
  
  public void setGastos(final List<Gasto> gastos) {
    this.gastos = gastos;
  }
  
  @Pure
  public Gasto getGasto() {
    return this.gasto;
  }
  
  public void setGasto(final Gasto gasto) {
    this.gasto = gasto;
  }
  
  @Pure
  public int getGastoParcial() {
    return this.gastoParcial;
  }
  
  public void setGastoParcial(final int gastoParcial) {
    this.gastoParcial = gastoParcial;
  }
  
  @Pure
  public int getIndice() {
    return this.indice;
  }
  
  public void setIndice(final int indice) {
    this.indice = indice;
  }
}
