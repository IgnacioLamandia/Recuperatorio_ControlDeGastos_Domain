import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors
import java.time.LocalDate

@Observable
@Accessors
class Gasto implements Cloneable {
	
    String descripcion
    LocalDate fechaRegistro
    int monto

    new(String descripcion, int monto){
        this.descripcion = descripcion
        this.fechaRegistro = LocalDate.now()
        this.monto = monto
    }
	
	new() {
		
		monto = 0
	}
    
    def copy() 
    { 
    	super.clone as Gasto 
    }
}