

import org.uqbar.commons.model.UserException
import java.util.List

class Calculador {
	
	Filtrador filtrador
	
	new(Filtrador filtrador){
		this.filtrador = filtrador
	}
	
	def calcularIndice(Gasto gastoSeleccionado, List<Gasto> gastos){
    	 
    	if(gastoSeleccionado.descripcion == null && gastoSeleccionado.monto == 0)
    	{
    		throw new UserException("Seleccione un gasto primero")
    	}
    	else
    	{	
    		filtrador.descripcion = gastoSeleccionado.descripcion
    		sumarGastosFiltrados(gastos)/gastos.size}
    	}
    
    def sumarGastosFiltrados(List<Gasto> gastosFiltrados)
    {
    	var res = 0   	
    	for(g : gastosFiltrados)
    	{
    		res = res + g.monto
    	}	    	
    	res
	}
}