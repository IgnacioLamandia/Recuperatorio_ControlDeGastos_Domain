import java.time.LocalDate
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.model.ObservableUtils
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class ControlDeGastosAppModel {

    RepoUsuarios repoUsuarios
    Usuario usuarioLogeado
    Filtrador filtrador
    Calculador calculador
    Gasto gastoSeleccionado
    List<Gasto> gastos
    Gasto gasto = new Gasto()
    int gastoParcial
    int indice

    new(RepoUsuarios repo) {
     
     this.repoUsuarios = repo
     this.filtrador = new Filtrador()
     gastoSeleccionado = new Gasto()
     calculador = new Calculador(filtrador)
      	
    }

    def void filtrar() {
      	ObservableUtils.firePropertyChanged(this, "gastos")
      	ObservableUtils.firePropertyChanged(this, "sumarGastosFiltrados")
     }
    
    def List<Gasto> getGastos() {
    	
    	filtrador.getGastos(usuarioLogeado.gastos)		
    }
    
    def sumarGastosFiltrados()
    {
    	var gastosFiltrados = this.getGastos() 	
    	calculador.sumarGastosFiltrados(gastosFiltrados)
	}
	
    def logearUsuario(Usuario usuario){
    	usuarioLogeado = usuario
    }
    
    def agregar()
    {
    	gasto.fechaRegistro = LocalDate.now()
    	this.usuarioLogeado.nuevoGasto(gasto)
    	gasto = new Gasto()
    	this.filtrar
    	gastoParcial = usuarioLogeado.gastoTotal
    }
    
    def calcularIndice()
    {
    	gastos = getGastos
    	indice = calculador.calcularIndice(gastoSeleccionado, gastos)	
    }

    def inicializar()
    {
    	this.filtrar
    	gastoParcial = usuarioLogeado.gastoTotal
    }
}