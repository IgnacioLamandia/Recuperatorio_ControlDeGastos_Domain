import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable
import java.util.List

@Observable
@Accessors
class Filtrador {

    String descripcion = ""

	def getGastos(List<Gasto> gastos){
	
		gastos.filter[ gasto | gasto.descripcion.toLowerCase().
    										  contains(descripcion.toLowerCase())
    										  ].toList
    }
    
}