import org.uqbar.commons.utils.Observable
import org.eclipse.xtend.lib.annotations.Accessors
import java.util.ArrayList
import java.util.List
import org.uqbar.commons.model.UserException

@Observable
@Accessors
class Usuario implements Cloneable{

    String nombre
	int gastoTotal
	List<Gasto> gastos
	Filtrador filtrador

    new(String nombre){
        this.nombre = nombre
        this.gastoTotal = 0
        this.gastos = new ArrayList<Gasto>()
        filtrador = new Filtrador()
    }
    
    def nuevoGasto(Gasto gasto)
    {
    	if(gasto.descripcion != "" && gasto.monto !=0 )
    	{
    		
    	this.gastos.add(gasto)
    	gastoTotal = gastoTotal + gasto.monto
    
    	} 
    	
    	else throw new UserException("Gasto invalido")
    }
    
    def copy() 
    { 
    	super.clone as Usuario 
    }

}