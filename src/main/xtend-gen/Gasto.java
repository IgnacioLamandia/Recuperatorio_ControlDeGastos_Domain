import java.time.LocalDate;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class Gasto {
  private Usuario usuario;
  
  private String descripcion;
  
  private LocalDate fechaRegistro;
  
  private int monto;
  
  public Gasto(final Usuario usuario, final String descripcion, final int monto) {
    this.usuario = usuario;
    this.descripcion = descripcion;
    LocalDate _now = LocalDate.now();
    this.fechaRegistro = _now;
    this.monto = monto;
  }
  
  @Pure
  public Usuario getUsuario() {
    return this.usuario;
  }
  
  public void setUsuario(final Usuario usuario) {
    this.usuario = usuario;
  }
  
  @Pure
  public String getDescripcion() {
    return this.descripcion;
  }
  
  public void setDescripcion(final String descripcion) {
    this.descripcion = descripcion;
  }
  
  @Pure
  public LocalDate getFechaRegistro() {
    return this.fechaRegistro;
  }
  
  public void setFechaRegistro(final LocalDate fechaRegistro) {
    this.fechaRegistro = fechaRegistro;
  }
  
  @Pure
  public int getMonto() {
    return this.monto;
  }
  
  public void setMonto(final int monto) {
    this.monto = monto;
  }
}
