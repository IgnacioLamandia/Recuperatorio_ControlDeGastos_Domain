import java.util.List;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;
import org.uqbar.commons.utils.Observable;

@Observable
@Accessors
@SuppressWarnings("all")
public class ControlDeGastosAppModel {
  private RepoUsuarios repoUsuarios;
  
  private Gasto gastoSeleccionado;
  
  private Usuario usuarioLogeado;
  
  private Filtrador filtrador;
  
  private String descripcionNueva;
  
  private int montoNuevo;
  
  public ControlDeGastosAppModel(final RepoUsuarios repo) {
    this.repoUsuarios = repo;
    Filtrador _filtrador = new Filtrador();
    this.filtrador = _filtrador;
  }
  
  public void buscar() {
    List<Gasto> _gastos = this.usuarioLogeado.getGastos();
    this.filtrador.filtrarGastosPorDescripcion(_gastos);
  }
  
  public Usuario logearUsuario(final Usuario usuario) {
    return this.usuarioLogeado = usuario;
  }
  
  public int agregar() {
    int _xblockexpression = (int) 0;
    {
      Gasto gastoNuevo = new Gasto(this.usuarioLogeado, this.descripcionNueva, this.montoNuevo);
      _xblockexpression = this.usuarioLogeado.nuevoGasto(gastoNuevo);
    }
    return _xblockexpression;
  }
  
  @Pure
  public RepoUsuarios getRepoUsuarios() {
    return this.repoUsuarios;
  }
  
  public void setRepoUsuarios(final RepoUsuarios repoUsuarios) {
    this.repoUsuarios = repoUsuarios;
  }
  
  @Pure
  public Gasto getGastoSeleccionado() {
    return this.gastoSeleccionado;
  }
  
  public void setGastoSeleccionado(final Gasto gastoSeleccionado) {
    this.gastoSeleccionado = gastoSeleccionado;
  }
  
  @Pure
  public Usuario getUsuarioLogeado() {
    return this.usuarioLogeado;
  }
  
  public void setUsuarioLogeado(final Usuario usuarioLogeado) {
    this.usuarioLogeado = usuarioLogeado;
  }
  
  @Pure
  public Filtrador getFiltrador() {
    return this.filtrador;
  }
  
  public void setFiltrador(final Filtrador filtrador) {
    this.filtrador = filtrador;
  }
  
  @Pure
  public String getDescripcionNueva() {
    return this.descripcionNueva;
  }
  
  public void setDescripcionNueva(final String descripcionNueva) {
    this.descripcionNueva = descripcionNueva;
  }
  
  @Pure
  public int getMontoNuevo() {
    return this.montoNuevo;
  }
  
  public void setMontoNuevo(final int montoNuevo) {
    this.montoNuevo = montoNuevo;
  }
}
